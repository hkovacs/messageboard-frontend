import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form;

  constructor(private fb: FormBuilder, private auth: AuthService) {
    this.form = fb.group({
      email: [''],
      password: [''],
    })
  }

  ngOnInit() {
  }

  loginData = {
    email: '',
    password: ''
  }
  login() {
    console.log('data', this.loginData);
    this.auth.login(this.loginData);
  }
}
